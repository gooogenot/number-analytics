package com.number.utils;

public class NumberAnalytics<T extends Number> {
    private Result<T> result = new Result<>();

    /**
     * @param number - added number
     * @return Result after method execution
     * @throws NullPointerException if number is null
     */
    public Result<T> addNumber(T number) {
        return addNumbersInner(number);
    }

    /**
     * @param numbers - added numbers
     * @return Result after method execution
     * @throws NullPointerException if some number is null
     */
    public Result<T> addNumbers(T... numbers) {
        return addNumbersInner(numbers);
    }

    /**
     * @return Min number or null if no one number has been added
     */
    public T getMin() {
        return result.min;
    }

    /**
     * @return Max number or null if no one number has been added
     */
    public T getMax() {
        return result.max;
    }

    /**
     * @return Average of all numbers or null if no one number has been added
     */
    public Double getAverage() {
        return result.average;
    }

    /**
     * Clear all results
     */
    public synchronized void clear() {
        result = new Result<>();
    }

    private synchronized Result<T> addNumbersInner(T... numbers) {
        T max = result.max;
        T min = result.min;
        double sum = 0.0;

        for (T number : numbers) {
            checkNotNull(number);
            double numberDouble = number.doubleValue();

            // Max calculate
            if (max == null) {
                max = number;
            } else if (Double.compare(max.doubleValue(), numberDouble) < 0) {
                max = number;
            }

            // Min calculate
            if (min == null) {
                min = number;
            } else if (Double.compare(min.doubleValue(), numberDouble) > 0) {
                min = number;
            }

            // Sum for average calculate
            sum += numberDouble;
        }

        // Average calculate
        double average;
        if (result.average == null) {
            average = sum / numbers.length;
        } else {
            average = (result.average * result.count + sum) / (result.count + numbers.length);
        }

        result = new Result<>(max, min, average, result.count + numbers.length);
        return result;
    }


    private void checkNotNull(T number) {
        if (number == null) {
            throw new NullPointerException();
        }
    }

    public static class Result<T> {
        private final T max;
        private final T min;
        private final Double average;

        private final long count;

        private Result() {
            this(null, null, null, 0);
        }

        private Result(T max, T min, Double average, long count) {
            this.max = max;
            this.min = min;
            this.average = average;
            this.count = count;
        }

        public T getMax() {
            return max;
        }

        public T getMin() {
            return min;
        }

        public Double getAverage() {
            return average;
        }
    }
}
