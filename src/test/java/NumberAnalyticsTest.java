import com.number.utils.NumberAnalytics;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NumberAnalyticsTest {
    private static final double EPSILON = 0.0000001;

    @Test
    public void emptyTest() {
        NumberAnalytics<Integer> analytics = new NumberAnalytics<>();
        Assert.assertNull(analytics.getMax());
        Assert.assertNull(analytics.getMin());
        Assert.assertNull(analytics.getAverage());
    }

    @Test(expected = NullPointerException.class)
    public void nullNumberTest() {
        NumberAnalytics<Integer> analytics = new NumberAnalytics<>();
        analytics.addNumber(null);
    }

    @Test(expected = NullPointerException.class)
    public void nullNumbersTest() {
        NumberAnalytics<Integer> analytics = new NumberAnalytics<>();
        analytics.addNumbers(1, 2, null);
    }

    @Test
    public void integerTest() {
        NumberAnalytics<Integer> analytics = new NumberAnalytics<>();
        List<Integer> sourceList = Arrays.asList(0, -100, 100, Integer.MAX_VALUE, Integer.MIN_VALUE);
        List<Integer> valueList = new ArrayList<>();

        for (Integer value : sourceList) {
            valueList.add(value);

            NumberAnalytics.Result<Integer> result = analytics.addNumber(value);

            Assert.assertEquals((Integer) valueList.stream().mapToInt(e -> e).max().getAsInt(), result.getMax());
            Assert.assertEquals((Integer) valueList.stream().mapToInt(e -> e).max().getAsInt(), analytics.getMax());

            Assert.assertEquals((Integer) valueList.stream().mapToInt(e -> e).min().getAsInt(), result.getMin());
            Assert.assertEquals((Integer) valueList.stream().mapToInt(e -> e).min().getAsInt(), analytics.getMin());

            Assert.assertTrue(doubleEquals(valueList.stream().mapToInt(e -> e).average().getAsDouble(), result.getAverage()));
            Assert.assertTrue(doubleEquals(valueList.stream().mapToInt(e -> e).average().getAsDouble(), analytics.getAverage()));
        }

        analytics.clear();
        Assert.assertNull(analytics.getMax());
        Assert.assertNull(analytics.getMin());
        Assert.assertNull(analytics.getAverage());
    }

    @Test
    public void bigDecimalTest() {
        NumberAnalytics<BigDecimal> analytics = new NumberAnalytics<>();
        analytics.addNumbers(BigDecimal.ZERO, BigDecimal.TEN.negate(), BigDecimal.TEN);

        Assert.assertEquals(BigDecimal.TEN, analytics.getMax());
        Assert.assertEquals(BigDecimal.TEN.negate(), analytics.getMin());
        Assert.assertTrue(doubleEquals(0.0, analytics.getAverage()));
    }

    @Test
    public void bigDecimalOverflowTest() {
        NumberAnalytics<BigDecimal> analytics = new NumberAnalytics<>();
        BigDecimal veryBigValue = new BigDecimal(Double.MAX_VALUE).add(new BigDecimal(Double.MAX_VALUE));
        analytics.addNumber(veryBigValue);

        Assert.assertEquals(0, veryBigValue.compareTo(analytics.getMax()));
        Assert.assertEquals(0, veryBigValue.compareTo(analytics.getMin()));
        Assert.assertEquals((Double) Double.POSITIVE_INFINITY, analytics.getAverage());
    }


    @Test
    public void integerParallelTest() throws InterruptedException {
        final int tCount = 5;

        NumberAnalytics<Integer> analytics = new NumberAnalytics<>();
        List<Integer> sourceList = IntStream.range(0, 100).boxed().collect(Collectors.toList());

        ExecutorService executor = Executors.newFixedThreadPool(tCount);

        for (int i = 0; i < tCount; i++) {
            executor.submit(() -> sourceList.forEach(analytics::addNumber));
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);

        double expectedAverage = IntStream.range(0, 100).sum() / 100.0;

        Assert.assertEquals((Integer) 99, analytics.getMax());
        Assert.assertEquals((Integer) 0, analytics.getMin());
        Assert.assertTrue(doubleEquals(expectedAverage, analytics.getAverage()));
    }


    private boolean doubleEquals(double d1, double d2) {
        return Math.abs(d1 - d2) < EPSILON;
    }
}
